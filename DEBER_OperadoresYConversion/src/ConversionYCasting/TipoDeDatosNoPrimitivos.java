package ConversionYCasting;

import java.io.Serializable;
import java.lang.reflect.Array;

public class TipoDeDatosNoPrimitivos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
				
		/////////////////////////////////CONVERSION DE REFERENCIA/////////////////////////////////
		////////////ASIGNACION
		//CLASES a CLASES
		Carro carro1 = new Carro("MAZDA");
		Vehiculo vehicuo1 = carro1;
		
		//INTERFACES a INTERFACES
		Interface interface1 = new Interface() {
			
			@Override
			public void metodo1() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void metodo2() {
				// TODO Auto-generated method stub
				
			}
		};
		
		SuperInterface superInterface1 = interface1;
		
		//CLASES a INTERFACES
		interface1 = carro1; 			//CLASE Carro implements Interface
		
		//con OBJECT a CLASE y a INTERFACES
		Object object1 = carro1;
		Object object2 = interface1;
		
		//ARRAY a OBJECT
		int [] intArray = {1, 2, 3};
		String [] strArray = {"1", "2", "3"};
		
		Object object3 = intArray;
		
		//ARRAY a Clonabe
		Cloneable cloneable1 = intArray;
		
		//ARRAY a Serializable
		Serializable serializable1 = intArray;		
		
		////////////POR METODO 
		Integer intO = 3;
		//metodo1(intO); //ERROR ----> para este caso Integer tendr�a que extender de Double
		metodo2(carro1);
		
		/////////////////////////////////CASTING DE REFERENCIA/////////////////////////////////
		Object ob = new Object();
		String [] stringarr = new String [50];
		Float floater = new Float(3.14f);
		
		ob = stringarr;
		ob = stringarr[5];
		//floater = ob;			//ERROR ----------------------- Float no es SUPERCLASE de OBJECT OJO
		floater = (Float) ob;	//Debe hacerse un CASTING
		ob = floater;			
	}
	
	static void metodo1 (Double dblO) {
		System.out.println("dblO: " + dblO);
	}
	
	static void metodo2 (Vehiculo vehiculoA){
		System.out.println("VehiculoA MArca: " + vehiculoA.strMarca);
	}

}


