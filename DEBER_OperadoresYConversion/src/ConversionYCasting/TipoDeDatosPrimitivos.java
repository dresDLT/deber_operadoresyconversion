package ConversionYCasting;

import java.io.Serializable;

public class TipoDeDatosPrimitivos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/***CASTING Y CONVERSIÓN***/
		char char1 = 'a';		//16 bits 		0 -- 2^16 		OJO SIN SIGNO 
		
		byte byte1 = 1; 		//8 bits		-2 ^7 -- 2 ^7 -1
		short short1 = 2; 		//16 bits
		int int1 = 3;			//32 bits
		long long1 = 4;			//64 bits
		
		float float1 = 3.45f;	//32 bits
		double double1 = 3.45;	//64 bits
		
		boolean bolean1 = true;
		
		
		/////////////////////////////////CONVERSION/////////////////////////////////
		////////////ASIGNACION
			double1 = float1 = long1 = int1 = short1 = byte1;
			double1 = float1 = long1 = int1 = char1;
	
		////////////POR METODO 
			metodo1(char1);
			//metodo2(double1); //ERROR -------- no puedo pasar de double a int OJO

		////////////PROMOCIÓN ARITMETICA
			double1 = int1 + float1;
			//short1 = byte1 + char1; //ERROR -- la suma de 2 datos numericos por lo menos es int OJO
			
			
		/////////////////////////////////CASTING /////////////////////////////////
			//OJO se puede pierde información
			byte1 = (byte) short1;
			short1 = (short) int1;
			char1 = (char) double1;

	}
	
	static void metodo1 (double dbl) {
		System.out.println("dblO: " + dbl);
	}

	static void metodo2 (int int1) {
		System.out.println("dblO: " + int1);
	}
}
