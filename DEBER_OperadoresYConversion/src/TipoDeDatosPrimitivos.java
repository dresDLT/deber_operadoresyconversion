
public class TipoDeDatosPrimitivos {

	public static void main(String[] args) {
		
		/***********************Operadores Primitivos***********************/
		
		/*********SIN SIGNO*********/
		//CHAR
		char char1 = 'a';
		char char2 = 98; //ASCII
		
		/*********CON SIGNO*********/
		//BYTE
		byte byte1 = 5, byte2 = -6;
		//SHORT
		short short1 = 2, short12 = -2;
		//INT
		int int1 = 3, int2 = 4;
		//LONG
		long long1 = 999999999, long2 = -999999999;
		
		/*********PUNTO FLOTANTE*********/
		//FLOAT
		float foat1 = 999999999999999999999999999999f,  float2 = -9999999999.999999999999999999999999f;
		//DOUBLE
		double double1 = 9.0, double2 = -9.999999999999999999999999999999;
		
		/*********BOOLEAN*********/
		boolean boolean1 = false, boolean2 = true;
		
		
		/***********************OPERADORES***********************/
		int int8 = 8;
		long long3 = -3;
		long long4 = int8 + long3;
		float float3 = int8 + long3;
		double double5 = int8 + long3;
		
		
		/*********UNARIOS*********/
		System.out.println("char1: " + ++char1);
		System.out.println("char2: " + ++char2);
		
		System.out.println("//---------------------------------------");		//PREGUNTAR
		//byte1 = 9 	00000101 ~ 11111010	= 
		//byte2 = -11	10000110 ~ 01111001 = 
		byte1 = -9; byte2 = -11;
		System.out.println("\nbyte1: "+ Integer.toBinaryString(byte1)+" = "+byte1+"      byte2: "+Integer.toBinaryString(byte2)+" = "+byte2);
		//byte byte3 = -byte2; //ERROR--------------------------------------OJO
		//byte byte3 = ~byte1; //ERROR--------------------------------------OJO
		System.out.println("byte1: "+ -byte1+"      byte2: "+ -byte2);
		System.out.println("byte1: "+ +byte1+"      byte2: "+ -byte2);
		System.out.println("--> byte1: "+ Integer.toBinaryString(~byte1)+" = "+~byte1+"      byte2: "+ Integer.toBinaryString(~byte2)+" = "+ ~byte2);
		System.out.println("--> byte1: "+ ~~byte1+"      byte2: "+ ~~byte2);
		
		
		System.out.println("//---------------------------------------");
		System.out.println("\nint1: " + int1);
		System.out.println("int2: " + int2);
		int int3 = ++int1 + int2++;	System.out.println("------int int3 = ++int1 + int2++;");
		System.out.println("int1: " + int1);
		System.out.println("int2: " + int2);
		System.out.println("int3: " + int3);
		
		int int4 = -9, int5 = -11;
		System.out.println("\nint4: "+ Integer.toBinaryString(int4)+" = "+int4+"      int5: "+Integer.toBinaryString(int5)+" = "+int5);
		//byte int4 = -int5; //ERROR--------------------------------------OJO
		//byte int4 = ~int5; //ERROR--------------------------------------OJO
		System.out.println("int4: "+ -int4+"      int5: "+ -int5);
		System.out.println("int4: "+ +int4+"      int5: "+ -int5);
		System.out.println("--> int4: "+ Integer.toBinaryString(~int4)+" = "+~int4+"      int5: "+ Integer.toBinaryString(~int5)+" = "+ ~int5);
		System.out.println("--> int4: "+ ~~int4+"      int5: "+ ~~int5);
		
		
		System.out.println("\n//---------------------------------------");
		//float1 = -9; byte2 = -11;
	
		//byte byte3 = -byte2; //ERROR--------------------------------------OJO
		//byte byte3 = ~byte1; //ERROR--------------------------------------OJO
		//System.out.println("float: "+ -float1+"      float2: "+ -float2); //ERROR--------------------------------------OJO
		//System.out.println("--> float1: "+ ~float1+"      float2: "+  ~float2); //ERROR--------------------------------------OJO
		//System.out.println("--> float1: "+ ~~float1+"      float2: "+ ~~float2); //ERROR--------------------------------------OJO
		
		
		System.out.println("\n//---------------------------------------");
		System.out.println("duble1: " + double1 + "        duble2: " + double2);
		double double3 = - double1;
		double double4 = - double2;
		//double double4 = ~ double1;	//ERROR--------------------------------------OJO
		System.out.println("duble3 (- double1): " + double3+"        duble4 (- double2): " + double4);
		
		System.out.println();
		
		int1 = ~ char1;
		int1 = - char1;
		
		int1 = ~ int2;
		int1 = - int2;
		
		long1 = ~ char1;
		long1 = - char1;
		
		long1 = ~ int8;
		long2 = - int8;
		
		long1 = ~ long2;
		long2 = - long1;
		
		float3 = ~ char1;
		float3 = - char1;
		
		float3 = ~ int8; 
		//float3 = ~ float2;			//ERROR--------------------------------------OJO	
		float3 = - float2;	
		
		float3 = ~ long1;
		float3 = - long1;
		
		double5 = ~ int8;
		double5 = - int8;
		
		double5 = ~ long1;
		double5 = - long1;
		
		//double5 = ~ float2;			//ERROR--------------------------------------OJO
		//double5 = - float 2;			//ERROR--------------------------------------OJO

		double5 = ++float2;
		
		//NEGAR UN BOOLEANO
		boolean1 = ! boolean2;

		
		/*********BINARIOS O ARITMETICOS***/
		System.out.println("\n//---------------------------------------");
		byte byte3 = 5;
		short short3 = 10;
		//short short4 = byte3 + short3;	//ERROR--------------------------------------OJO
		int int6 = byte3 + short3;
		
		char char3 = 5;
		byte byte4 = -5;
		//byte byte5 = byte4 + char3;		//ERROR--------------------------------------OJO

		int int7 = byte4 + char3;			//la suma de dos datos numericos por lo menos es INT
				
		System.out.println("int8: " + int8+"    long3: "+long3);
		System.out.println("long4: "+long4+"    float3: "+float3+"    double5: "+double5);

		int8 = (int) double5;
		System.out.println("int8 = (int) double5: "+int8);
		
		float3 = int8;
		System.out.println("float3 = int8: "+float3);
				
		int1 = byte1 + char3;
		int1 = short1 + byte1;
		int1 = int1 + char2;
		long1 = long1 + int2;
		
		float3 = long1 + int2;
		double1 = long1 - int2;
		double1 = long1 * int2;
		double1 = long1 / int2;
		double1 = int2 / long1;
		double1 = int2 % long1;
		
		
		/*//OJO DIVIDIR PARA CERO y %
		int int9 = 5;
		int int10 = 0;
		
		int int11 = int9 / int10;	//NO HAY ERROR DE COMPILACIÓN --> ERROR DE EJECUCION
		int int12 = int9 % int10;	//NO HAY ERROR DE COMPILACIÓN --> ERROR DE EJECUCION
		
		System.out.println("-------------------DIVIDIR PA' CERO-------------------");
		System.out.println("\n int int9 = 5; int int10 = 0" );
		System.out.println("\n int int11 = int9 / int1; int int12 = int9 % int1" );
		System.out.println("\n int int11 = " + int11 + "    int int12 = " + int12 );*/
		
		
		
		/***COMPARACIÓN***/
		float float1 = 2.33f;
		System.out.println("\n-------------------COMPARACION-------------------");
		if (int1 > char1 ) {
			if (double1 < float1) {
				if (short1 >= long1) {
					if (byte1 <= double1) {
						if (int1 == char1) {
							System.out.println("SIN");
						}
						else if (int1 != char1) {
							System.out.println("CON");
						}
					}
				}
			}
		} else{
			System.out.println("---->HECHO \n");
		}
		
		/***BITWISE***/
		System.out.println("\n-------------------BITWISE-------------------");
		int int11 = 17;
		long long5 = 20;
		
		long long6 = int11 & long5;	//OPERADOR AND 
		long long7 = int11 ^ long5; //OPERADOR OR EXCLUSIVO
		long long8 = int11 | long5; //OPERADOR OR
		
		System.out.println("int int11 = 17                 long long5 = 20;");
		System.out.println("int11: " + Integer.toBinaryString(int11));
		System.out.println("long5: " + Integer.toBinaryString((int)long5));
		
		System.out.println("long long6 = int11 & long5; ---> " + long6);
		System.out.println("long6: " + Integer.toBinaryString((int)long6));
		
		System.out.println("long long7 = int11 ^ long5; ---> " + long7);
		System.out.println("long7: " + Integer.toBinaryString((int)long7));
		
		System.out.println("long long8 = int11 | long5; ---> " + long8);
		System.out.println("long8: " + Integer.toBinaryString((int)long8));
		
		/***OPERADORES BOOLEANOS***/
		System.out.println("\n-------------------OPERADORES BOOLEANOS-------------------");
		
		if (char1 == byte1 && char2 == byte2) {			//ANALIZA SOLO UN LADO
			if (short1 == int1 || short1 == int2) {		//ANALIZA SOLO UN LADO
				System.out.println("SIN");
			}
		} else {
			if (short1 != int1 | short1 != int2) {		//ANALIZA AMBOS LADOS
				if (char1 != byte1 & char2 != byte2) {	//ANALIZA AMBOS LADOS
					if (char1 != byte1 ^ char2 != byte2) {	//ANALIZA AMBOS LADOS
						System.out.println("CON");
					}
				}
			}
		}
		System.out.println("---> HECHO");
		
		//CASTING y CONVERSION
		
		//---------CASTING------------------
		long a = 4;
		int b = (int) a;
		
		//---------ASIGNACION------------------
		a = b; //long = int
		
		//---------METODO-----------------
		metodoC(b);
		
		//---------PROMOCION ARITMETICA----------------
		long l = a + b;
		
	}
	
	static void metodoC (long long1){
		System.out.println(long1);
	}
	
}
