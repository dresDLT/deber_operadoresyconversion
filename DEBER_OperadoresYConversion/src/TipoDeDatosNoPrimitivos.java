

public class TipoDeDatosNoPrimitivos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*********UNARIOS***/
		System.out.println("/*********UNARIOS***/");
		
		Integer integer2 = 4;
		String string5 = "a";	//Operadores Unarios solo para tipoDeDatosOBJETOS NUMERICOS 
		
		System.out.println("integer2: "+ integer2);
		System.out.println("integer2++: "+ integer2++);
		System.out.println("integer2: "+ integer2);
		System.out.println("++integer2: "+ ++integer2);
		System.out.println("integer2: "+ integer2);
		System.out.println("~integer2: "+ ~integer2);
		System.out.println("integer2: "+ integer2);
		System.out.println("-integer2: "+ -integer2);
		System.out.println("integer2: "+ integer2);
		//System.out.println("!integer2: "+ !integer2);	//ERROR

		
		/*********BINARIOS O ARITMETICOS***/
		System.out.println("\n\n/*********BINARIOS O ARITMETICOS***/");
		
		//Operaci�n + --> por lo menos uno debe ser String --> por lo tanto el resultado ser� String
		
		Integer integer1 = 3;
		Double doubleO1 = 3.4;
		String string1 = "PELOTA";
		
		String string2 = integer1 + string1;
		String string3 = integer1 + doubleO1 + string1; 
		String string4 = string2 + string3;
		//String string5 = integer1 + doubleO1; 	//ERROR------->por lo menos debe sumarse un String
		
		Double doubleO2 = integer1 + doubleO1;
		
		System.out.println("string2 = integer1 + string1---> " + string2);
		System.out.println("string3 = integer1 + doubleO1 + string1---> " + string3);
		System.out.println("String string4 = string2 + string3---> " + string4);
		
		
		/***BITWISE***/
		System.out.println("\n-------------------BITWISE-------------------");
		Integer intO11 = 17;
		Double doubleO5 = 20.0;
		
		/*******************ERROR*******************/
		//Double doubleO6 = intO11 & doubleO5;	//OPERADOR AND 
		//Double doubleO7 = intO11 ^ doubleO5; //OPERADOR OR EXCLUSIVO
		//Double doubleO8 = intO11 | doubleO5; //OPERADOR OR
				

	}

}
